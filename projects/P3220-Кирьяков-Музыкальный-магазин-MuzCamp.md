# Музыкальный магазин MuzCamp
## Автор
P3220 Кирьяков Анатолий Анатольевич

## Описание
Сайт является площадкой для продажи музыки и мерча. Пользователи смогут покупать товары, писать отзыв на купленное, редактировать личную страницу, выставлять на продажу музыку и мерч, образовывать группы и подписываться на других пользователей. На главной странице будут показаны самые популярные артисты, альбомы и группы, и возможно будет сортировка по тегам.