# Сервис для организации подарков семьям на рождение ребенка «WhatDay». 

## Авторы
P3222 Зайцев Данил Дмитриевич    
P3222 Декстер Максим Павлович
P3222 Джатиев Юрий Александрович 

## Описание
Сервис для семьи, ожидающей ребенка. Позволяет друзьям этой семьи делать на рождение детей более организованные подарки, а, например, не дарить деньги в конверте, а также отправлять поздравления и пожелания. Проходит все в формате ставки в календаре на предполагаемую дату рождения, деньги идут родителям, а угадавшие получают подарки от сервиса. Заказчик будет иметь личный кабинет, возможность выбрать шаблон сайта с календарем, просматривать сделанные ставки-поздравления, даритель может заполнить поздравление и произвести оплату.

## Детали
* Продукт - веб-приложение c базой данных, пользовательский интерфейс - веб-страницы
* Стек технологий - NodeJs, React, MySQL
